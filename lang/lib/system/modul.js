const { Module, addStlModule, stl } = require("../../vm");
const fs = require("fs");

let modul = new Module();
addStlModule("comes::system::mod", modul);

modul.method("Moduł", (_, mod, name) => {
  if (!name.startsWith("comes::") && name != "comes") {
    if (!stl.has(name)) {
      addStlModule(name, mod);
    } else {
      stl.get(name).load(mod);
    }
  } else {
    throw new EvalError(`Moduł() : Nielegalna nazwa "${name}".`);
  }
})
