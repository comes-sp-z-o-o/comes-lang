const { Module, addStlModule } = require("../../vm");

let matematyka = new Module();
addStlModule("comes::system::matematyka", matematyka);

matematyka.method("Neg", (_vm, _mod, a) => -a);
matematyka.method("Nie", (_vm, _mod, a) => !a);

matematyka.method("Pl", (_vm, _mod, ...arg) => arg.reduce((a, c) => a + c));
matematyka.method("Mi", (_vm, _mod, ...arg) => arg.reduce((a, c) => a - c));
matematyka.method("Ra", (_vm, _mod, ...arg) => arg.reduce((a, c) => a * c));
matematyka.method("Pr", (_vm, _mod, ...arg) => arg.reduce((a, c) => a / c));

matematyka.method("Ró", (_vm, _mod, a, b) => a == b);
matematyka.method("Nró", (_vm, _mod, a, b) => a != b);
matematyka.method("Wi", (_vm, _mod, a, b) => a > b);
matematyka.method("Mn", (_vm, _mod, a, b) => a < b);
matematyka.method("WiR", (_vm, _mod, a, b) => a >= b);
matematyka.method("MnR", (_vm, _mod, a, b) => a <= b);

matematyka.method("I", (_vm, _mod, a, b) => a && b);
matematyka.method("Lub", (_vm, _mod, a, b) => a || b);

matematyka.method("Pot", (_vm, _mod, a, b) => a ** b);
matematyka.method("Modulo", (_vm, _mod, a, b) => a % b);
matematyka.method("Bezwzględna", (_vm, _mod, x) => Math.abs(x));

matematyka.method("Sinus", (_vm, _mod, x) => Math.sin(x));
matematyka.method("Kosinus", (_vm, _mod, x) => Math.cos(x));
matematyka.method("Tangens", (_vm, _mod, x) => Math.tan(x));

matematyka.method("Losuj", (_vm, _mod) => Math.random());
