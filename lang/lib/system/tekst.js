const { Module, addStlModule } = require("../../vm");

let tekst = new Module();
addStlModule("comes::system::tekst", tekst);

tekst.method("Łącz", (_vm, _mod, ...str) => str.join(""));
