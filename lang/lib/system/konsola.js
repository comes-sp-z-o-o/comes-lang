const { Module, addStlModule } = require("../../vm");

let konsola = new Module();
addStlModule("comes::system::konsola", konsola);

konsola.method("Drukuj", (vm, _mod, ...message) => {
  if (!vm.stdout) {
    vm.stdout = "";
  }
  let msg = message.join("");
  vm.stdout += msg + '\n';
  console.log(msg);
});

konsola.method("Pisz", (vm, _mod, ...message) => {
  if (!vm.stdout) {
    vm.stdout = "";
  }
  let msg = message.join("");
  vm.stdout += msg;
  process.stdout.write(msg);
})
