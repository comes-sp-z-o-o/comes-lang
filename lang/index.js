module.exports = {
  version: "0.2.0",
  vm: require("./vm"),
  tokenizer: require("./tokenizer"),
  parser: require("./parser")
}

module.exports.changelog = `
**comes/lang ${module.exports.version}** - język skryptowania do dupy™
dokumentacja: https://gitlab.com/comes-sp-z-o-o/comes-lang/blob/master/README.md
 · prefix » zamiast tagowania bota, bo wygodniej
 · errory nie crashuja bota
 · \`comes::system\`; wszystko stamtad jest importowane domyslnie
 · \`comes::system::mod\`
   w tym metoda \`Moduł\` ktora tworzy lub modyfikuje modul (szczegoly w doku)`
