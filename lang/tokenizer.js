class Tokenizer {
  constructor(input) {
    this.input = input;
    this.pos = 0;
  }

  get textPos() {
    let ln = 0;
    let col = 0;
    for (let i = 0; i < this.pos; i++) {
      let char = this.input[i];
      if (char == "\n") {
        ln++;
        col = 0;
      }
      col++;
    }
    return { ln, col };
  }

  error(message) {
    let pos = this.textPos;
    throw new SyntaxError(`${pos.ln}:${pos.col} ${message}`);
  }

  get atEnd() {
    return this.pos >= this.input.length;
  }

  peek(amt = 1) {
    return this.input.substring(this.pos, this.pos + amt);
  }

  next(amt = 1) {
    this.pos += amt;
  }

  consume(amt = 1) {
    const chars = this.peek(amt);
    this.next(amt);
    return chars;
  }

  match(literal) {
    if (this.peek(literal.length) == literal) {
      this.next(literal.length);
      return true;
    }
    return false;
  }

  ignore() {
    while (true) {
      if (/[ \t\r\n\f]/.test(this.peek(1))) {
        this.next(1);
        continue;
      }
      if (this.match("[")) {
        while (!this.match("]")) {
          if (this.atEnd) {
            this.error("Niedokończony komentarz !");
          }
          this.next(1);
        }
        continue;
      }
      break;
    }
  }

  number() {
    let num = "";
    while (/[0-9]/.test(this.peek(1))) {
      num += this.consume(1);
    }
    if (this.match(".")) {
      num += ".";
      while (/[0-9]/.test(this.peek(1))) {
        num += this.consume(1);
      }
    }
    if (num.length > 0) {
      return { type: "number", num: parseFloat(num) };
    }
  }

  string() {
    if (this.match('"')) {
      let str = "";
      while (!this.match('"')) {
        str += this.consume(1);
        if (this.atEnd) this.error("Niedokończony ciąg znaków !");
      }
      return { type: "string", str };
    }
  }

  ident() {
    let ident = "";
    while (/[a-zA-Z0-9_\u0080-\uFFFF]/.test(this.peek(1))) {
      ident += this.consume(1);
    }
    if (ident.length > 0) {
      return { type: "ident", ident };
    }
  }

  keyword() {
    let ident = this.ident();
    if (ident != null) {
      switch (ident.ident) {
        case "ustal":   return { type: "kwLet" };
        case "ustaw":   return { type: "kwSet" };
        case "metoda":  return { type: "kwMethod" };
        case "jezeli":
        case "jeżeli":  return { type: "kwIf" };
        case "kiedy":   return { type: "kwWhile" };
        case "dla":     return { type: "kwFor" };
        case "rob":
        case "rób":     return { type: "kwDo" };
        case "koniec":  return { type: "kwEnd" };
        case "wczytaj": return { type: "kwImport" };
        case "NIC":     return { type: "kwNull" };
        case "PRAWDA":
        case "TAK":     return { type: "kwTrue" };
        case "FAŁSZ":
        case "NIE":     return { type: "kwFalse" };
        default: return ident;
      }
    }
  }

  paren() {
    let par = this.peek(1);
    let type = "";
    switch (par) {
      case "(": type = "lPar"; break;
      case ")": type = "rPar"; break;
    }
    if (type != "") {
      this.next(1);
      return { type };
    }
  }

  punct() {
    let type = "";
    if (this.match("*")) type = "punctStar";
    else if (this.match(","))  type = "punctComma";
    else if (this.match("::")) type = "punctDColon";
    if (type != "") {
      return { type };
    }
  }

  tryAll(rules) {
    let token;
    for (let rule of rules) {
      token = this[rule]();
      if (token != null) return token;
    }
    if (token == null) {
      this.error(`Niedozwolony znak : '${this.peek(1)}'`);
    }
  }

  tokenize() {
    let tokens = [];
    this.ignore();
    while (!this.atEnd) {
      let pos = this.textPos;
      let tok = this.tryAll([
        "number",
        "string",
        "keyword",
        "paren",
        "punct"
      ]);
      tok.pos = pos;
      tokens.push(tok);
      this.ignore();
    }
    return tokens;
  }
}

module.exports = Tokenizer;
