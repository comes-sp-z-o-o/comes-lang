const Tokenizer = require("./tokenizer.js");
const Parser = require("./parser.js");
const fs = require("fs");

function loadStl() {
  let stl = new Map();
  let stlRaw = require("../stl.json");
  for (let name in stlRaw) {
    let mod = new Module();
    let modRaw = stlRaw[name];
    for (let name in modRaw.methods) {
      mod.method(name, modRaw.methods[name]);
    }
    stl.set(name, mod);
  }
  return stl;
}

function saveStl() {
  let mods = {};
  for (let [name, mod] of stl) {
    if (name[0] == ".") {
      continue;
    }
    let m = {
      methods: {}
    };
    for (let [name, method] of mod.methods) {
      if (typeof method == "object") {
        m.methods[name] = method;
      }
    }
    mods[name] = m;
  }
  fs.writeFile("stl.json", JSON.stringify(mods), () => {});
}

class VM {
  constructor() {
    this.modules = stl;
    this.scopes = [];
    this.onNodeStart = () => {};
    this.onNodeEnd = () => {};
  }

  error(message) {
    throw new EvalError(message);
  }

  pushScope() {
    this.scopes.push(new Map());
  }

  popScope() {
    this.scopes.pop();
  }

  lookup(mod, name) {
    for (let i = this.scopes.length - 1; i >= 0; i--) {
      if (this.scopes[i].has(name)) return this.scopes[i].get(name);
    }
    if (mod.globals.has(name)) {
      return mod.globals.get(name);
    }
    return undefined;
  }

  declare(mod, name, val) {
    if (this.scopes.length > 0) {
      let scope = this.scopes[this.scopes.length - 1];
      scope.set(name, val);
    } else {
      mod.globals.set(name, val)
    }
  }

  evalAST(node, mod, args = []) {
    this.onNodeStart();
    switch (node.type) {
      case "null": return null;
      case "bool": return node.bool;
      case "number": return node.num;
      case "string": return node.str;

      case "ref":
        let val = this.lookup(mod, node.variable);
        if (val == undefined) {
          this.error(`Zmienna ${node.variable} NIE ISTNIEJE !!!`);
        }
        return val;
      case "arg": return args[node.id - 1];

      case "let": {
        let val = this.lookup(mod, node.name);
        if (val != undefined) {
          this.error(`Zmienna ${node.name} już istnieje !`);
        }
        this.declare(mod, node.name, this.evalAST(node.val, mod, args));
        return;
      };
      case "set": {
        let val = this.lookup(mod, node.name);
        if (val == undefined) {
          this.error(`Zmienna ${node.name} NIE ISTNIEJE !!!`);
        }
        this.declare(mod, node.name, this.evalAST(node.val, mod, args));
        return;
      };

      case "do": {
        this.pushScope();
        this.evalList(node.body, mod, args);
        this.popScope();
        return;
      }
      case "if": {
        this.pushScope();
        if (this.evalAST(node.cond, mod, args)) {
          this.evalList(node.body, mod, args);
        }
        this.popScope();
        return;
      };
      case "while": {
        this.pushScope();
        while (this.evalAST(node.cond, mod, args)) {
          this.evalList(node.body, mod, args);
        }
        this.popScope();
        return;
      };
      case "for": {
        this.pushScope();
        let min = this.evalAST(node.min);
        let max = this.evalAST(node.max);
        let inc = this.evalAST(node.inc);
        if (max > min) {
          for (let i = min; i < max; i += inc) {
            this.declare(mod, node.variable, i);
            this.evalList(node.body, mod, args);
          }
        } else if (max < min) {
          for (let i = max - 1; i >= min; i -= inc) {
            this.declare(mod, node.variable, i);
            this.evalList(node.body, mod, args);
          }
        }
        this.popScope();
        return;
      }

      case "call": {
        let cargs = [];
        for (let exp of node.args) {
          cargs.push(this.evalAST(exp, mod, args));
        }

        if (mod.methods.has(node.method)) {
          let method = mod.methods.get(node.method);
          if (typeof method == "function") {
            return method.apply(null, [this, mod].concat(cargs));
          } else if (typeof method == "object") {
            this.evalAST(method, mod, cargs);
            return this.lookup(mod, "WYNIK");
          } else {
            this.error(`Nie można wywołać : ${(typeof method).toUpperCase()} !`);
          }
        } else {
          this.error(`Metoda '${node.method}' NIE ISTNIEJE !!!`);
        }
      };

      case "method": {
        mod.method(node.name, node.body);
        return;
      };

      case "script": {
        return this.evalList(node.script, mod, []);
      };

      case "import": {
        if (this.modules.has(node.namespace)) {
          let imported = this.modules.get(node.namespace);
          mod.load(imported);
        } else {
          this.error(`Moduł ${node.namespace} NIE ISTNIEJE !!!`);
        }
        return;
      };

      default: {
        console.log(node);
        this.error(`Niezaimplementowany węzeł : ${node.type}`);
      };
    }
    this.onNodeEnd();
  }

  loadStl(mod) {
    for (let [k, v] of stl) {
      if (k.startsWith("comes::system::")) {
        mod.load(v);
      }
    }
  }

  evalList(list, mod, args = []) {
    for (let node of list) {
      this.evalAST(node, mod, args);
    }
  }

  eval(modName, script) {
    let tokenizer = new Tokenizer(script);
    let parser = new Parser(tokenizer.tokenize());
    let ast = parser.parse();
    let mod = new Module();
    this.loadStl(mod);
    this.modules.set(modName, mod);
    let result = this.evalAST(ast, mod, []);
    saveStl();
    return result;
  }
}

class Module {
  constructor() {
    this.globals = new Map();
    this.methods = new Map();
  }

  method(name, impl) {
    this.methods.set(name, impl);
  }

  load(other) {
    for (let [k, v] of other.globals) {
      this.globals.set(k, v);
    }
    for (let [k, v] of other.methods) {
      this.methods.set(k, v);
    }
  }

  call(method, vm, ...args) {
    if (this.methods.has(method)) {
      let m = this.methods.get(method);
      if (typeof m == "function") {
        return m.apply(null, [vm, this].concat(args));
      } else if (typeof m == "object") {
        vm.evalAST(m, this, args);
        return vm.lookup(this, "WYNIK");
      } else {
        vm.error(`Nie można wywołać : ${(typeof m).toUpperCase()} !`);
      }
    }
  }
}

var stl = loadStl();

function addStlModule(name, mod) {
  stl.set(name, mod);
}

module.exports = { VM, Module, addStlModule, stl };

require("./lib/_loader");
